package com.mapswithme.maps.widget.placepage;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


public class PlacePageVisibilityProxy implements BasePlacePageAnimationController.OnVisibilityChangedListener
{
  @NonNull
  private final BasePlacePageAnimationController.OnVisibilityChangedListener mListener;

  public PlacePageVisibilityProxy(@NonNull BasePlacePageAnimationController.OnVisibilityChangedListener listener)
  {
    mListener = listener;
  }

  @Override
  public void onPreviewVisibilityChanged(boolean isVisible)
  {
    mListener.onPreviewVisibilityChanged(isVisible);

  }

  @Override
  public void onPlacePageVisibilityChanged(boolean isVisible)
  {
    mListener.onPlacePageVisibilityChanged(isVisible);
  }
}
